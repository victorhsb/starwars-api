package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/victorhsb/starwars-api/config"
	"github.com/victorhsb/starwars-api/domain"
	"github.com/victorhsb/starwars-api/repository"
)

type rawResponse struct {
	Count   int64           `json:"count"`
	Results []swapiResponse `json:"results"`
}

type swapiResponse struct {
	Name    string   `json:"name"`
	Climate string   `json:"climate"`
	Terrain string   `json:"terrain"`
	Films   []string `json:"films"`
}

func main() {
	response, err := http.Get("https://swapi.co/api/planets/")
	if err != nil {
		log.Fatal(err)
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	var responsePlanets rawResponse
	err = json.Unmarshal(body, &responsePlanets)
	if err != nil {
		log.Fatal(err)
	}

	if responsePlanets.Results != nil {
		repository := repository.Must(config.Environment.Database.URL, config.Environment.Database.Name)

		parseSWApi := func(rp swapiResponse) *domain.Planet {
			movieAmount := int64(len(rp.Films))
			return &domain.Planet{
				Name:    rp.Name,
				Movies:  &movieAmount,
				Terrain: strings.Split(rp.Terrain, ","),
				Climate: strings.Split(rp.Climate, ","),
			}
		}

		for _, rp := range responsePlanets.Results {
			if got, err := repository.LookupByName(rp.Name); err == nil {
				planet := parseSWApi(rp)
				planet.SetID(got.ID)

				repository.Update(planet)
				fmt.Println("updated old planet")
				continue
			}

			planet := parseSWApi(rp)
			created, err := repository.Create(planet)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println("created new planet. id: ", created.ID)
		}
	}
}
