package main

import (
	"log"
	"net/http"

	"go.uber.org/zap"

	"github.com/victorhsb/starwars-api/config"
	"github.com/victorhsb/starwars-api/handler"
	"github.com/victorhsb/starwars-api/repository"
	"github.com/victorhsb/starwars-api/service"
)

func main() {
	// set the log format to JSON and log the starting of the server
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal(err)
	}
	defer logger.Sync()
	logger.Info("starting server...")

	// Start the repository layer
	rep := repository.Must(config.Environment.Database.URL, config.Environment.Database.Name)

	// Start the Service Layer responsible for communicating w/ the repository layer and doing any service logic necessary
	svc := service.New(rep)

	// Start the handlers using the router, and the service
	router := handler.StartRouter(svc)

	// Start the server, at last!
	server := &http.Server{
		Handler: router,
		Addr:    config.Environment.Server.Addr,
	}

	// Make it listen
	if err := server.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
