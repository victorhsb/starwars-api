package domain

import (
	"fmt"

	"github.com/Kamva/mgm/v2"
)

type Planet struct {
	mgm.DefaultModel `bson:",inline"`
	Name             string   `json:"name" bson:"name"`
	Climate          []string `json:"climate" bson:"climate"`
	Terrain          []string `json:"terrain" bson:"terrain"`
	Movies           *int64   `json:"movies,omitempty" bson:"movies"`
}

func (p *Planet) String() string {
	return fmt.Sprintf("name[%s] climate[%v] terrain[%v] movies[%v]", p.Name, p.Climate, p.Terrain, p.Movies)
}

func (Planet) Collection() string {
	return "planets"
}

// ValidateCreation checks if the user is submitting a valid planet
func (c *Planet) ValidateCreation() bool {
	if c.Name == "" {
		return false
	}
	// we can have 0 movies, no information at all, but never less than 0 movies (which is impossible)
	if c.Movies != nil && *c.Movies < 0 {
		return false
	}

	// ensure unique items
	c.Terrain = unique(c.Terrain)
	c.Climate = unique(c.Climate)
	return true
}

// trying to ensure the default call, as the created_at dates were not updating properly
func (p *Planet) Creating() error {
	return p.DefaultModel.Creating()
}

// used when updating, with omitempty values so only what has been passed is updated indeed
type UpdatePlanet struct {
	Name    *string   `json:"name,omitempty"`
	Climate *[]string `json:"climate,omitempty"`
	Terrain *[]string `json:"terrain,omitempty" bson:"terrain"`
	Movies  *int64    `json:"movies,omitempty" bson:"movies"`
}

// ValidateUpdate checks if the user is submitting a valid planet when updating (name is not required in this case)
func (c *UpdatePlanet) ValidateUpdate() bool {
	if c.Name != nil && *c.Name == "" {
		// no empty names
		return false
	}

	if c.Movies != nil && *c.Movies < 0 {
		return false
	}

	// ensure unique items
	if c.Terrain != nil {
		t := unique(*c.Terrain)
		c.Terrain = &t
	}
	if c.Climate != nil {
		t := unique(*c.Climate)
		c.Climate = &t
	}
	return true
}

// return a unified array of string
func unique(sl []string) []string {
	keys := make(map[string]bool)
	list := []string{}
	for _, entry := range sl {
		if _, ok := keys[entry]; !ok {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}
