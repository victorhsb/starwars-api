package domain

import "errors"

var (
	// ErrNotFound is used when the information you are looking for could not be found or is unexistent
	ErrNotFound = errors.New("document not found or not existent")
	// ErrStub is a friendly error that tells us when the repository struct is the stub one.
	ErrStub = errors.New("Stub interface")
	// ErrUnknownScheme is used when the repository URL does not contain one of the supported schemes (mongodb, stub)
	ErrUnknownScheme = errors.New("unsuported or unknown connection scheme")
)
