package repository

import "github.com/victorhsb/starwars-api/domain"

type PlanetStub struct {
	LookupCalledWith string
	LookupPlanet     *domain.Planet
	LookupError      error

	LookupByNameCalledWith string
	LookupByNamePlanet     *domain.Planet
	LookupByNameError      error

	ListPlanets []*domain.Planet
	ListError   error

	UpdateCalledWith *domain.Planet
	UpdateError      error

	CreateCalledWith *domain.Planet
	CreatePlanet     *domain.Planet
	CreateError      error

	DeleteCalledWith string
	DeleteError      error
}

func (p *PlanetStub) Lookup(id string) (*domain.Planet, error) {
	p.LookupCalledWith = id
	return p.LookupPlanet, p.LookupError
}

func (p *PlanetStub) LookupByName(name string) (*domain.Planet, error) {
	p.LookupByNameCalledWith = name
	return p.LookupByNamePlanet, p.LookupByNameError
}

func (p *PlanetStub) List() ([]*domain.Planet, error) {
	return p.ListPlanets, p.ListError
}

func (p *PlanetStub) Update(update *domain.Planet) error {
	p.UpdateCalledWith = update

	return p.UpdateError
}

func (p *PlanetStub) Create(create *domain.Planet) (*domain.Planet, error) {
	p.CreateCalledWith = create

	return p.CreatePlanet, p.CreateError
}

func (p *PlanetStub) Delete(id string) error {
	p.DeleteCalledWith = id

	return p.DeleteError
}
