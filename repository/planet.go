package repository

import (
	"net/url"
	"time"

	"github.com/Kamva/mgm/v2"
	"github.com/kpango/glg"
	"github.com/pkg/errors"

	"go.mongodb.org/mongo-driver/mongo/options"

	"github.com/victorhsb/starwars-api/domain"
)

type Interface interface {
	Lookup(id string) (*domain.Planet, error)
	LookupByName(name string) (*domain.Planet, error)
	List() ([]*domain.Planet, error)
	Update(update *domain.Planet) error
	Create(create *domain.Planet) (*domain.Planet, error)
	Delete(id string) error
}

func Must(connStr string, dbname string) Interface {
	i, err := New(connStr, dbname)
	if err != nil {
		glg.Fatal(err)
	}

	return i
}

func New(connStr string, dbname string) (Interface, error) {
	parsedURL, err := url.Parse(connStr)
	if err != nil {
		return nil, errors.Wrap(err, "could not parse connection URL")
	}

	switch parsedURL.Scheme {
	case "mongodb":

		err := mgm.SetDefaultConfig(&mgm.Config{CtxTimeout: time.Second * 6}, dbname)
		if err != nil {
			return nil, errors.Wrap(err, "could not set default mongodb config")
		}

		if client, err := mgm.NewClient(
			options.Client().ApplyURI(connStr),
		); err != nil {
			return nil, err
		} else {
			db := client.Database(dbname)
			return &PlanetDB{mgm.NewCollection(db, domain.Planet{}.Collection())}, nil
		}
	case "stub":
		// to prevent unintentional stub usage as it is for unit tests only
		return &PlanetStub{
			LookupError:       domain.ErrStub,
			LookupByNameError: domain.ErrStub,
			CreateError:       domain.ErrStub,
			ListError:         domain.ErrStub,
			UpdateError:       domain.ErrStub,
			DeleteError:       domain.ErrStub,
		}, nil
	}

	return nil, domain.ErrUnknownScheme
}
