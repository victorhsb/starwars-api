package repository

import (
	"context"

	"github.com/Kamva/mgm/v2"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/victorhsb/starwars-api/domain"
)

type PlanetDB struct {
	db *mgm.Collection
}

func (p *PlanetDB) Lookup(id string) (*domain.Planet, error) {
	var planet domain.Planet

	err := p.db.FindByID(id, &planet)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, domain.ErrNotFound
		}
		return nil, errors.Wrap(err, "could not lookup planet")
	}

	return &planet, nil
}

func (p *PlanetDB) LookupByName(name string) (*domain.Planet, error) {
	planet := &domain.Planet{}

	err := p.db.First(bson.M{"name": name}, planet)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, domain.ErrNotFound
		}
		return nil, errors.Wrap(err, "could not lookup planet by name")
	}

	return planet, nil
}

func (p *PlanetDB) List() ([]*domain.Planet, error) {
	var planets []*domain.Planet

	err := p.db.SimpleFind(&planets, bson.D{})
	if err != nil {
		if err == mongo.ErrNilDocument {
			return nil, domain.ErrNotFound
		}
		return nil, errors.Wrap(err, "could not list planets")
	}

	return planets, nil
}

func (p *PlanetDB) Update(update *domain.Planet) error {
	return p.db.Update(update)
}

func (p *PlanetDB) Create(create *domain.Planet) (*domain.Planet, error) {
	result, err := p.db.InsertOne(context.TODO(), create)
	if err != nil {
		return nil, errors.Wrap(err, "could not create planet")
	}

	create.SetID(result.InsertedID)

	return create, nil
}

func (p *PlanetDB) Delete(id string) error {
	planet := &domain.Planet{}
	prepID, err := planet.PrepareID(id)
	if err != nil {
		return err
	}
	planet.SetID(prepID)

	err = p.db.Delete(planet)
	if err != nil {
		return errors.Wrap(err, "could not delete")
	}

	return nil
}
