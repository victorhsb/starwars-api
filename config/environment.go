package config

import (
	"log"

	"github.com/vrischmann/envconfig"
)

var Environment struct {
	Server struct {
		Addr string `envconfig:"default=:8081"`
	}

	Database struct {
		URL  string `envconfig:"default=mongodb://anakin:padme@localhost:27017"`
		Name string `envconfig:"default=sw-api"`
	}
}

func init() {
	if err := envconfig.InitWithOptions(&Environment, envconfig.Options{Prefix: "SW", AllOptional: true}); err != nil {
		log.Fatal(err)
	}
}
