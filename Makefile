
SW_API_DATABASE_USER:=anakin
SW_API_DATABASE_PWD:=padme
SW_API_DATABASE_PORT:=27017

SW_API_DATABASE_HOST:="mongodb://$(SW_API_DATABASE_USER):$(SW_API_DATABASE_PWD)@localhost:$(SW_API_DATABASE_PORT)"
SW_API_DATABASE_NAME:="sw-api"
SW_API_SERVER_ADDR:=":8081"
SW_API_DOCKER_IMAGE_NAME:=starwars-api-server:latest

populate:
	@go run cmd/starwars-api-loader/main.go

init-db:
	@docker run -d --rm -e MONGO_INITDB_ROOT_USERNAME=$(SW_API_DATABASE_USER) -e MONGO_INITDB_ROOT_PASSWORD=$(SW_API_DATABASE_PWD) -p $(SW_API_DATABASE_PORT):27017 --name sw-mongodb mongo

run:
	@go run cmd/starwars-api-server/main.go

test:
	@go test ./...

release:
	@go mod vendor
	@docker build -t $(SW_API_DOCKER_IMAGE_NAME) .
	@rm -rf vendor/

start-test-server:
	@which reflex || go install github.com/cespare/reflex
	@which gotest || go install github.com/rakyll/gotest
	@$(shell reflex -r '\.go$/' -s -- sh -c "clear && gotest -test.testfail -cover -coverprofile cover.out ./..." )
