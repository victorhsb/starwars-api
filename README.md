# starwars-api

## Technology
The entire project is made using Golang and Docker. 
A few dependencies are bound to it functioning but can be easily downloaded running a ```go mod download``` command.

## Database
The database chosen here is MongoDB and its document based storage, and you can start it running the make task 
```make db-init```
and then running a 
```make populate```
so it downloads the updated data from the official swapi.co public API making the necessary changes.

## The API
The API can be run by executing the ```make run``` task, which will run it directly from the main go file, or by executing
the ```make release``` to create a docker image ready for production usage.

### using the API
Here are the endpoints available for use at this API:
- ```curl -x GET http://{HOST}/swapi/v1//planet/id/{id}``` -> Returns the planet bound to the ID
- ```curl -x GET http://{HOST}/swapi/v1/planet/name/{name}``` -> Returns the first planet with this name
- ```curl -x GET http://{HOST}/swapi/v1/planets ->``` Returns all the planets on the database
- ```curl -x POST http://{HOST}/swapi/v1/planet -d '{ "name": "batata", "terrain":["plain"], "climate":["hot"], "movies":1 }'``` -> Creates a new planet with the given data and returns what has been created (with its new id).
- ```curl -X PUT http://{HOST}/swapi/v1/planet/id/{id} -d { "name": "newname", "climate":["new climate"] }``` -> updates the planet (given by id) with the new information given, merging with the old so it only updates what has changed
- ```curl -x DELETE http://{HOST}/swapi/v1/planet/id/{id}``` -> delete the planet with the given id
