package service

import (
	"github.com/pkg/errors"

	"github.com/victorhsb/starwars-api/domain"
	"github.com/victorhsb/starwars-api/repository"
)

type Service struct {
	repository repository.Interface
}

func New(repo repository.Interface) *Service {
	return &Service{repo}
}

func (s *Service) Lookup(id string) (*domain.Planet, error) {
	return s.repository.Lookup(id)
}

func (s *Service) LookupByName(name string) (*domain.Planet, error) {
	return s.repository.LookupByName(name)
}

func (s *Service) List() ([]*domain.Planet, error) {
	return s.repository.List()
}

func (s *Service) Update(id string, updatePlanet *domain.UpdatePlanet) error {
	update := &domain.Planet{}
	// to ensure that no preexistent information is lost, we should merge what didn`t change
	if old, err := s.Lookup(id); err == nil {
		if updatePlanet.Name == nil {
			update.Name = old.Name
		} else {
			update.Name = *updatePlanet.Name
		}

		if updatePlanet.Climate == nil {
			update.Climate = old.Climate
		} else {
			update.Climate = *updatePlanet.Climate
		}

		if updatePlanet.Terrain == nil {
			update.Terrain = old.Terrain
		} else {
			update.Terrain = *updatePlanet.Terrain
		}

		if updatePlanet.Movies != nil {
			update.Movies = updatePlanet.Movies
		} else {
			update.Movies = old.Movies
		}
	} else {
		// we can`t update a planet that does not exist
		// other than not found we may ignore it
		if err == domain.ErrNotFound {
			return err
		}
	}

	prepid, err := update.PrepareID(id)
	if err != nil {
		return errors.Wrap(err, "could not prepare planet id")
	}

	update.SetID(prepid)

	return s.repository.Update(update)
}

func (s *Service) Create(create *domain.Planet) (*domain.Planet, error) {
	return s.repository.Create(create)
}

func (s *Service) Delete(id string) error {
	return s.repository.Delete(id)
}
