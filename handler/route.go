package handler

import (
	"log"
	"net/http"
	"runtime/debug"
	"time"

	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"github.com/victorhsb/starwars-api/service"
)

type Route struct {
	Name        string           `json:"name"`
	Path        string           `json:"path"`
	Method      string           `json:"method"`
	HandlerFunc http.HandlerFunc `json:"-"`
}

var routes []Route

func StartRouter(svc *service.Service) *mux.Router {
	// Start the router to be used by the handlers
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal(err)
	}
	defer logger.Sync()
	router := mux.NewRouter()

	StartHandlers(svc)

	for _, route := range routes {
		handler := route.HandlerFunc

		handler = MiddlewareRecover(
			MiddlewareLogger(
				handler,
			),
		)

		router.Methods(route.Method).Path(route.Path).Name(route.Name).Handler(handler)

		logger.Sugar().Infow("added route", "route", route)
	}

	// reset the routes that were registered already
	routes = routes[:0]

	return router
}

func MiddlewareRecover(next http.HandlerFunc) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		logger, _ := zap.NewProduction()
		defer logger.Sync()
		defer func() {
			if rvr := recover(); rvr != nil {
				logger.Fatal("could not recover", zap.Binary("panic", debug.Stack()))
				defer logger.Sync()
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
		}()
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

func MiddlewareLogger(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger, _ := zap.NewProduction()
		defer logger.Sync()
		start := time.Now()
		lrw := newLoggingResponseWriter(w)
		next.ServeHTTP(lrw, r)
		elapsed := time.Since(start)

		clientIP := r.RemoteAddr
		if xff := r.Header.Get("X-Forwarded-For"); xff != "" {
			clientIP = xff
		}

		var userAgent string
		if ua := r.Header.Get("User-Agent"); ua != "" {
			userAgent = ua
		}

		logger.Sugar().Infow(http.StatusText(lrw.statusCode),
			"userAgent", userAgent,
			"elapsedf", elapsed.Seconds(),
			"path", r.URL.RequestURI(),
			"clientIP", clientIP,
			"referer", r.Referer(),
			"requestURI", r.RequestURI,
			"remoteAddr", r.RemoteAddr,
			"method", r.Method,
			"statusCode", lrw.statusCode,
			"uri", r.URL,
		)
	})
}

type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

func newLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
	return &loggingResponseWriter{w, http.StatusOK}
}

func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}
