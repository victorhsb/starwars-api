package handler_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-playground/assert"

	"github.com/victorhsb/starwars-api/domain"
	"github.com/victorhsb/starwars-api/handler"
	"github.com/victorhsb/starwars-api/repository"
	"github.com/victorhsb/starwars-api/service"
)

func TestLookup(t *testing.T) {
	defaultPlanet := &domain.Planet{Name: "potato", Climate: []string{"potatoes"}}
	testCases := []struct {
		Name               string
		Path               string
		GivenError         error
		GivenPlanet        *domain.Planet
		GivenID            string
		ExpectedPlanet     *domain.Planet
		ExpectedStatusCode int
	}{
		{
			Name:               "without errors",
			Path:               "/swapi/v1/planet/id/",
			GivenError:         nil,
			GivenPlanet:        defaultPlanet,
			GivenID:            "5e7b8cbdc5d552205f07e29c",
			ExpectedPlanet:     defaultPlanet,
			ExpectedStatusCode: http.StatusOK,
		},
		{
			Name:               "with error",
			Path:               "/swapi/v1/planet/id/",
			GivenID:            "123qwe",
			GivenError:         errors.New("sample error"),
			ExpectedStatusCode: http.StatusInternalServerError,
		},
		{
			Name:               "without id",
			Path:               "/swapi/v1/planet/",
			ExpectedStatusCode: http.StatusNotFound,
		},
		{
			Name:               "with wrong path",
			Path:               "/swapi/v1/planet/",
			ExpectedStatusCode: http.StatusNotFound,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			repo := repository.Must("stub://", "test").(*repository.PlanetStub)

			repo.LookupError = tc.GivenError
			repo.LookupByNameError = tc.GivenError
			repo.LookupPlanet = tc.GivenPlanet
			repo.LookupByNamePlanet = tc.GivenPlanet

			svc := service.New(repo)

			r := handler.StartRouter(svc)

			// Prepare request and response recorders
			rec := httptest.NewRecorder()
			req := httptest.NewRequest("GET", tc.Path+tc.GivenID, nil)

			// Serve the http request in a controlled environment
			r.ServeHTTP(rec, req)

			if tc.ExpectedPlanet != nil {
				planet := &domain.Planet{}

				err := json.NewDecoder(rec.Result().Body).Decode(planet)
				if err != nil {
					t.Error(err)
				}

				assert.Equal(t, planet, tc.ExpectedPlanet)
			}

			assert.Equal(t, rec.Result().StatusCode, tc.ExpectedStatusCode)
			if tc.GivenID != "" {
				assert.Equal(t, repo.LookupCalledWith, tc.GivenID)
			}
		})
	}
}

func TestLookupByName(t *testing.T) {
	defaultPlanet := &domain.Planet{Name: "potato", Climate: []string{"potatoes"}}
	testCases := []struct {
		Name               string
		Path               string
		GivenError         error
		GivenPlanet        *domain.Planet
		GivenID            string
		ExpectedPlanet     *domain.Planet
		ExpectedStatusCode int
	}{
		{
			Name:               "by name without errors",
			Path:               "/swapi/v1/planet/name/",
			GivenError:         nil,
			GivenPlanet:        defaultPlanet,
			GivenID:            "123qwe",
			ExpectedPlanet:     defaultPlanet,
			ExpectedStatusCode: http.StatusOK,
		},
		{
			Name:               "by name with error",
			Path:               "/swapi/v1/planet/name/",
			GivenID:            "123qwe",
			GivenError:         errors.New("sample error"),
			ExpectedStatusCode: http.StatusInternalServerError,
		},
		{
			Name:               "without name",
			Path:               "/swapi/v1/planet/name/",
			ExpectedStatusCode: http.StatusNotFound,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			repo := repository.Must("stub://", "test").(*repository.PlanetStub)

			repo.LookupByNameError = tc.GivenError
			repo.LookupByNamePlanet = tc.GivenPlanet

			svc := service.New(repo)

			r := handler.StartRouter(svc)

			// Prepare request and response recorders
			rec := httptest.NewRecorder()
			req := httptest.NewRequest("GET", tc.Path+tc.GivenID, nil)

			// Serve the http request in a controlled environment
			r.ServeHTTP(rec, req)

			if tc.ExpectedPlanet != nil {
				planet := &domain.Planet{}

				err := json.NewDecoder(rec.Result().Body).Decode(planet)
				if err != nil {
					t.Error(err)
				}

				assert.Equal(t, planet, tc.ExpectedPlanet)
			}

			assert.Equal(t, rec.Result().StatusCode, tc.ExpectedStatusCode)
			if tc.GivenID != "" {
				assert.Equal(t, repo.LookupByNameCalledWith, tc.GivenID)
			}
		})
	}
}

func TestList(t *testing.T) {
	defaultPlanets := []*domain.Planet{
		{Name: "potato", Climate: []string{"potatoes"}},
		{Name: "carrot", Climate: []string{"carrots"}},
	}
	testCases := []struct {
		Name               string
		Path               string
		GivenError         error
		GivenPlanets       []*domain.Planet
		ExpectedPlanets    []*domain.Planet
		ExpectedStatusCode int
	}{
		{
			Name:               "without errors",
			Path:               "/swapi/v1/planets",
			GivenError:         nil,
			GivenPlanets:       defaultPlanets,
			ExpectedPlanets:    defaultPlanets,
			ExpectedStatusCode: http.StatusOK,
		},
		{
			Name:               "with error",
			Path:               "/swapi/v1/planets",
			GivenError:         errors.New("sample error"),
			ExpectedStatusCode: http.StatusInternalServerError,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			repo := repository.Must("stub://", "test").(*repository.PlanetStub)

			repo.ListError = tc.GivenError
			repo.ListPlanets = tc.GivenPlanets

			svc := service.New(repo)

			r := handler.StartRouter(svc)

			// Prepare request and response recorders
			rec := httptest.NewRecorder()
			req := httptest.NewRequest("GET", tc.Path, nil)

			// Serve the http request in a controlled environment
			r.ServeHTTP(rec, req)

			if tc.ExpectedPlanets != nil {
				var planet []*domain.Planet

				err := json.NewDecoder(rec.Result().Body).Decode(&planet)
				if err != nil {
					t.Error(err)
				}

				assert.Equal(t, planet, tc.ExpectedPlanets)
			}

			assert.Equal(t, rec.Result().StatusCode, tc.ExpectedStatusCode)
		})
	}
}

func TestDelete(t *testing.T) {
	testCases := []struct {
		Name               string
		Path               string
		GivenError         error
		GivenID            string
		ExpectedStatusCode int
	}{
		{
			Name:               "without errors",
			Path:               "/swapi/v1/planet/id/",
			GivenID:            "123qwe",
			GivenError:         nil,
			ExpectedStatusCode: http.StatusNoContent,
		},
		{
			Name:               "with error",
			Path:               "/swapi/v1/planet/id/",
			GivenID:            "123qwe",
			GivenError:         errors.New("sample error"),
			ExpectedStatusCode: http.StatusInternalServerError,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			repo := repository.Must("stub://", "test").(*repository.PlanetStub)

			repo.DeleteError = tc.GivenError

			svc := service.New(repo)

			r := handler.StartRouter(svc)

			// Prepare request and response recorders
			rec := httptest.NewRecorder()
			req := httptest.NewRequest("DELETE", tc.Path+tc.GivenID, nil)

			// Serve the http request in a controlled environment
			r.ServeHTTP(rec, req)

			assert.Equal(t, rec.Result().StatusCode, tc.ExpectedStatusCode)
			if tc.GivenID != "" {
				assert.Equal(t, repo.DeleteCalledWith, tc.GivenID)
			}
		})
	}
}

func TestUpdate(t *testing.T) {
	defaultPlanet := &domain.Planet{Name: "potato", Climate: []string{"potatoes"}}
	testCases := []struct {
		Name               string
		Path               string
		GivenError         error
		GivenPlanet        *domain.Planet
		GivenLookupPlanet  *domain.Planet
		GivenID            string
		ExpectedPlanet     *domain.Planet
		ExpectedStatusCode int
	}{
		{
			Name:               "without errors",
			Path:               "/swapi/v1/planet/id/",
			GivenID:            "5e7b8cbdc5d552205f07e29c",
			GivenPlanet:        defaultPlanet,
			GivenLookupPlanet:  &domain.Planet{Name: "toast", Terrain: []string{"foo", "bar"}},
			ExpectedPlanet:     &domain.Planet{Name: "potato", Climate: []string{"potatoes"}, Terrain: []string{"foo", "bar"}},
			GivenError:         nil,
			ExpectedStatusCode: http.StatusNoContent,
		},
		{
			Name:               "with error",
			Path:               "/swapi/v1/planet/id/",
			GivenID:            "5e7b8cbdc5d552205f07e29c",
			GivenPlanet:        defaultPlanet,
			GivenError:         errors.New("sample error"),
			ExpectedStatusCode: http.StatusInternalServerError,
		},
		{
			Name:               "with no body",
			Path:               "/swapi/v1/planet/id/",
			GivenID:            "5e7b8cbdc5d552205f07e29c",
			GivenPlanet:        nil,
			ExpectedStatusCode: http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			repo := repository.Must("stub://", "test").(*repository.PlanetStub)

			repo.UpdateError = tc.GivenError
			repo.LookupError = tc.GivenError
			repo.LookupPlanet = tc.GivenLookupPlanet

			svc := service.New(repo)

			r := handler.StartRouter(svc)

			var b []byte
			if tc.GivenPlanet != nil {
				var err error
				b, err = json.Marshal(tc.GivenPlanet)
				if err != nil {
					t.Error(err)
				}
			}

			// Prepare request and response recorders
			rec := httptest.NewRecorder()
			req := httptest.NewRequest("PUT", tc.Path+tc.GivenID, bytes.NewReader(b))
			req.Header.Add("Content-Type", "application/json")

			// Serve the http request in a controlled environment
			r.ServeHTTP(rec, req)

			assert.Equal(t, rec.Result().StatusCode, tc.ExpectedStatusCode)
			if tc.ExpectedPlanet != nil {
				// add the given ID to the expected planet
				id, _ := tc.ExpectedPlanet.PrepareID(tc.GivenID)
				tc.ExpectedPlanet.SetID(id)
				// expect it to be called like this
				assert.Equal(t, repo.UpdateCalledWith, tc.ExpectedPlanet)
			}
		})
	}
}

func TestCreate(t *testing.T) {
	defaultPlanet := &domain.Planet{Name: "potato", Climate: []string{"potatoes"}}
	testCases := []struct {
		Name               string
		Path               string
		GivenError         error
		GivenPlanet        *domain.Planet
		ExpectedStatusCode int
	}{
		{
			Name:               "without errors",
			Path:               "/swapi/v1/planet",
			GivenPlanet:        defaultPlanet,
			GivenError:         nil,
			ExpectedStatusCode: http.StatusCreated,
		},
		{
			Name:               "with error",
			Path:               "/swapi/v1/planet",
			GivenPlanet:        defaultPlanet,
			GivenError:         errors.New("sample error"),
			ExpectedStatusCode: http.StatusInternalServerError,
		},
		{
			Name:               "with slash at path",
			Path:               "/swapi/v1/planet/",
			GivenError:         nil,
			ExpectedStatusCode: http.StatusNotFound,
		},
		{
			Name:               "with no body",
			Path:               "/swapi/v1/planet",
			GivenPlanet:        nil,
			ExpectedStatusCode: http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			repo := repository.Must("stub://", "test").(*repository.PlanetStub)

			repo.CreateError = tc.GivenError
			repo.CreatePlanet = tc.GivenPlanet

			svc := service.New(repo)

			r := handler.StartRouter(svc)

			var b []byte
			if tc.GivenPlanet != nil {
				var err error
				b, err = json.Marshal(tc.GivenPlanet)
				if err != nil {
					t.Error(err)
				}
			}

			// Prepare request and response recorders
			rec := httptest.NewRecorder()
			req := httptest.NewRequest("POST", tc.Path, bytes.NewReader(b))
			req.Header.Add("Content-Type", "application/json")

			// Serve the http request in a controlled environment
			r.ServeHTTP(rec, req)

			assert.Equal(t, rec.Result().StatusCode, tc.ExpectedStatusCode)
			if tc.GivenPlanet != nil {
				assert.Equal(t, repo.CreateCalledWith, tc.GivenPlanet)
			}
		})
	}
}
