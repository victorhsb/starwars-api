package handler

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"github.com/victorhsb/starwars-api/domain"
	"github.com/victorhsb/starwars-api/service"
	. "github.com/victorhsb/starwars-api/service"
)

type Handler struct {
	service *Service
}

var path = "/swapi/v1"

// Starts the struct with the service and append the routes to be later initialized
func StartHandlers(svc *service.Service) {
	h := Handler{svc}
	routes = append(routes,
		Route{"lookupPlanetByName", path + "/planet/name/{name}", "GET", h.HandleLookupByName},
		Route{"lookupPlanet", path + "/planet/id/{id}", "GET", h.HandleLookup},
		Route{"listPlanet", path + "/planets", "GET", h.HandleList},
		Route{"updatePlanet", path + "/planet/id/{id}", "PUT", h.HandleUpdate},
		Route{"createPlanet", path + "/planet", "POST", h.HandleCreate},
		Route{"deletePlanet", path + "/planet/id/{id}", "DELETE", h.HandleDelete},
	)
}

// HandleLookup returns the planet by using only the id
func (h *Handler) HandleLookup(w http.ResponseWriter, r *http.Request) {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	// get the id to be looked up
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		// could not get id, should return bad request
		Response(w, http.StatusBadRequest, nil)
		return
	}

	// gets the planet to be returned
	planet, err := h.service.Lookup(id)
	if err != nil {
		if err == domain.ErrNotFound {
			Response(w, http.StatusNotFound, nil)
			return
		}
		logger.Error("could not lookup planet", zap.Error(err))
		Response(w, http.StatusInternalServerError, nil)
		return
	}

	ResponseJSON(w, http.StatusOK, planet)
}

// HandleLookupByName returns the planet by searching by ID
func (h *Handler) HandleLookupByName(w http.ResponseWriter, r *http.Request) {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	// get the name to be looked up
	vars := mux.Vars(r)
	name, ok := vars["name"]
	if !ok {
		// could not get name, should return bad request
		Response(w, http.StatusBadRequest, nil)
		return
	}

	// gets the planet
	planet, err := h.service.LookupByName(name)
	if err != nil {
		if err == domain.ErrNotFound {
			Response(w, http.StatusNotFound, nil)
			return
		}
		logger.Error("could not lookup planet by name", zap.Error(err))
		Response(w, http.StatusInternalServerError, []byte(http.StatusText(http.StatusInternalServerError)))
		return
	}

	ResponseJSON(w, http.StatusOK, planet)
}

// HandleList returns a list of planets, not filtered
// NOTE: this can be improved with pagination, sorting and filtering, which can demand some time
func (h *Handler) HandleList(w http.ResponseWriter, r *http.Request) {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	planets, err := h.service.List()
	if err != nil {
		logger.Error("could not list planets", zap.Error(err))
		Response(w, http.StatusInternalServerError, nil)
		return
	}

	ResponseJSON(w, http.StatusOK, planets)
}

// HandleUpdate updates an existing planet
func (h *Handler) HandleUpdate(w http.ResponseWriter, r *http.Request) {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	// get the id of the planet to be updated
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		// could not get name, should return bad request
		Response(w, http.StatusBadRequest, nil)
		return
	}

	// gets the planet from the request body
	var planet *domain.UpdatePlanet
	if ok := GetJSON(w, r, &planet); !ok {
		logger.Warn("could not decode body")
		return
	}
	// validate the body received
	if ok := planet.ValidateUpdate(); !ok {
		Response(w, http.StatusBadRequest, nil)
		return
	}

	// effectively update the planet. if the planet can`t be found, a 404 will be returned and the client may create a new one via POST method
	err := h.service.Update(id, planet)
	if err != nil {
		if err == domain.ErrNotFound {
			Response(w, http.StatusNotFound, nil)
			return
		}
		logger.Warn("could not update planet", zap.Error(err))
		Response(w, http.StatusInternalServerError, nil)
		return
	}

	Response(w, http.StatusNoContent, nil)
}

// HandleCreate creates a new planet
func (h *Handler) HandleCreate(w http.ResponseWriter, r *http.Request) {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	// parses the body to the planet
	var planet *domain.Planet
	if ok := GetJSON(w, r, &planet); !ok {
		logger.Warn("could not decode body")
		return
	}

	// create the planet
	created, err := h.service.Create(planet)
	if err != nil {
		logger.Warn("could not create planet", zap.Error(err))
		Response(w, http.StatusInternalServerError, err) // TODO: should not return the error. debugging only
		return
	}

	ResponseJSON(w, http.StatusCreated, created)
}

// HandleDelete deletes an existing planet
func (h *Handler) HandleDelete(w http.ResponseWriter, r *http.Request) {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		// could not get id, should return bad request
		Response(w, http.StatusBadRequest, nil)
		return
	}

	err := h.service.Delete(id)
	if err != nil {
		logger.Error("could not lookup planet", zap.Error(err))
		Response(w, http.StatusInternalServerError, nil)
		return
	}

	Response(w, http.StatusNoContent, nil)
}

// Response is a shortcut that returns an HTTP response
func Response(w http.ResponseWriter, status int, data interface{}) {
	if data == nil {
		w.WriteHeader(status)
		return
	}
	ResponseJSON(w, status, data)
}

// ResponseJSON is a shortcut that renders an HTTP response in JSON format
func ResponseJSON(w http.ResponseWriter, status int, data interface{}) {
	logger, _ := zap.NewProduction()
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(status)
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		logger.Error("could not encode response", zap.Error(err))
	}
}

// GetJSON decodes the request body into the given data type
func GetJSON(w http.ResponseWriter, r *http.Request, data interface{}) bool {
	err := json.NewDecoder(r.Body).Decode(data)
	if err != nil {
		Response(w, http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return false
	}
	return true
}
