FROM golang as build

ADD . /go/src/github.com/victorhsb/starwars-api
WORKDIR /go/src/github.com/victorhsb/starwars-api

RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 \
        go build -v -a -installsuffix cgo \
        -o sw-api-linux-amd64 \
        github.com/victorhsb/starwars-api/cmd/starwars-api-server

FROM alpine:latest
COPY --from=build /go/src/github.com/victorhsb/starwars-api/sw-api-linux-amd64 /bin/
RUN apk update && \
        apk add tzdata && \
        apk add ca-certificates && \
        cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && \
        rm -rf /var/cache/apk/*

ENTRYPOINT ["sw-api-linux-amd64"]
