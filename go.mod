module github.com/victorhsb/starwars-api

go 1.13

require (
	github.com/Kamva/mgm/v2 v2.0.0
	github.com/cespare/reflex v0.2.0 // indirect
	github.com/fatih/color v1.9.0 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-playground/assert v1.2.1
	github.com/gorilla/mux v1.7.4
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51 // indirect
	github.com/kpango/glg v1.5.1
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/ogier/pflag v0.0.1 // indirect
	github.com/pkg/errors v0.9.1
	github.com/rakyll/gotest v0.0.0-20200206190159-3023d5d6366c // indirect
	github.com/vrischmann/envconfig v1.2.0
	go.mongodb.org/mongo-driver v1.3.1
	go.uber.org/zap v1.14.1
	golang.org/x/sys v0.0.0-20200323222414-85ca7c5b95cd // indirect
)
